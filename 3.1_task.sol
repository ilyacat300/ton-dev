pragma ton-solidity >= 0.35.0;
pragma AbiHeader expire;

contract line {
    string[] public users;
    
    function addUser(string val) public {
        users.push(val);
    }
    
    function next() public {
        string[] save;
        uint i = 1;
        if(users.length == 1) {users = save;}
        while(i < users.length) {
            save.push(users[i]);
            i++;
        }
        users = save;
    }
}
