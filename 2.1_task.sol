pragma ton-solidity >= 0.35.0;
pragma AbiHeader expire;

contract line {
    string[] public users;
    
    function addUser(string memory val) public {
        users.push(val);
    }
    
    function next() public {
        string[] storage save;
        uint i = 0;
        while(i < users.length) {
            save.push(users[i]);
            i++;
        }
        users = save;
    }
}
